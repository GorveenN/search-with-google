use reqwest::header::USER_AGENT;
use reqwest::Client;
use thiserror::Error;

use select::document::Document;
use select::predicate::*;

use std::result::Result;
use std::str;

///Error returned if search fails
#[derive(Error, Debug)]
pub enum Error {
    #[error("request failed")]
    ReqwestError(#[from] reqwest::Error),
    #[error("io error")]
    IoError(#[from] std::io::Error),
}

///SearchResult struct represents a single search result
#[derive(Debug, Clone)]
pub struct SearchResult {
    pub link: String,
    pub title: String,
    pub description: String,
    pub description_raw: String,
}

async fn get_text_body(
    url: &str,
    agent: &str,
    client: Option<&Client>,
) -> Result<String, reqwest::Error> {
    match client {
        Some(client) => {
            client
                .get(url)
                .header(USER_AGENT, agent)
                .send()
                .await?
                .text()
                .await
        }
        None => {
            Client::new()
                .get(url)
                .header(USER_AGENT, agent)
                .send()
                .await?
                .text()
                .await
        }
    }
}

pub(crate) async fn google(
    query: &str,
    limit: u32,
    agent: String,
    client: Option<&Client>,
) -> Result<Vec<SearchResult>, Error> {
    let request_string = format!(
        "https://www.google.com/search?q={}&gws_rd=ssl&num={}",
        query, limit
    );
    let mut sections: Vec<SearchResult> = Vec::new();

    let document = Document::from(
        get_text_body(&request_string, &agent, client)
            .await?
            .as_str(),
    );

    for node in document.find(
        Attr("id", "search")
            .descendant(Attr("id", "rso"))
            .descendant(Class("g"))
            .descendant(Class("rc")),
    ) {
        let mut link = String::new();
        if let Some(a) = node.find(Class("r").child(Name("a"))).next() {
            if let Some(l) = a.attr("href") {
                link = l.to_string();
            }
        }
        let mut description = String::new();
        let mut description_raw = String::new();
        if let Some(desc) = node
            .find(Class("s").descendant(Name("span").and(Class("st"))))
            .next()
        {
            description_raw = desc.inner_html();
            for child in desc.children() {
                let frag = scraper::Html::parse_fragment(&child.html());

                for node in frag.tree {
                    if let scraper::node::Node::Text(text) = node {
                        description.push_str(&format!("{}", text.text));
                    }
                }
            }
        }
        let mut title = String::new();
        if let Some(new_node) = node.find(Class("LC20lb")).next() {
            title = new_node.text();
        }
        sections.push(SearchResult {
            title,
            link,
            description,
            description_raw,
        });
    }
    Ok(sections)
}
